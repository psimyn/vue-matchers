import { shallowMount } from '@vue/test-utils';
import toHaveEmitted from '../src/have_emitted';

describe('toHaveEmitted', () => {
  beforeAll(() => {
    expect.extend({ toHaveEmitted });
  });

  const DummyComponent = {
    name: 'DummyComponent',
    template: '<div>dummy</div>',
  };

  it('fails for garbage', () => {
    expect(() => {
      expect('garbage').toHaveEmitted('smell');
    }).toThrow();
  });

  describe('expecting event', () => {
    it('passes if event is emitted', () => {
      const wrapper = shallowMount(DummyComponent);

      wrapper.vm.$emit('someEvent');

      expect(wrapper).toHaveEmitted('someEvent');
    });

    it('fails if no event is emitted', () => {
      const wrapper = shallowMount(DummyComponent);

      expect(() => {
        expect(wrapper).toHaveEmitted('someEvent');
      }).toThrow();
    });

    it('fails if the wrong event is emitted', () => {
      const wrapper = shallowMount(DummyComponent);

      wrapper.vm.$emit('wrongEvent');

      expect(() => {
        expect(wrapper).toHaveEmitted('someEvent');
      }).toThrow();
    });
  });

  describe('expecting noevent', () => {
    it('fails if event is emitted', () => {
      const wrapper = shallowMount(DummyComponent);

      wrapper.vm.$emit('someEvent');

      expect(() => {
        expect(wrapper).not.toHaveEmitted('someEvent');
      }).toThrow();
    });

    it('passes if no event is emitted', () => {
      const wrapper = shallowMount(DummyComponent);

      expect(wrapper).not.toHaveEmitted('someEvent');
    });

    it('passes if wrong event is emitted', () => {
      const wrapper = shallowMount(DummyComponent);

      wrapper.vm.$emit('wrongEvent');

      expect(wrapper).not.toHaveEmitted('someEvent');
    });
  });
});
