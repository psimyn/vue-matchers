import { Wrapper } from "@vue/test-utils";

export default function toHaveEmitted(wrapper, eventName) {
  if (!(wrapper instanceof Wrapper)) {
    throw new Error(this.utils.printWithType('wrapper', wrapper, this.utils.printReceived));
  }

  const eventNames = Object.keys(wrapper.emitted());
  const pass = eventNames.includes(eventName);

  if (pass) {
    const eventCount = wrapper.emitted()[eventName].length;
    return {
      pass,
      message: () => `Expected event "${eventName}" not to be emitted but was emitted ${eventCount} times!`,
    }
  }

  if (eventNames.length === 0) {
    return {
      pass,
      message: () => `Expected event "${eventName}" to be emitted!`,
    }
  }

  const message = () => {
    const matcherHint =     this.utils.matcherHint('toHaveEmitted');
    const expected = `Expected: ${this.utils.printExpected(eventName)}`;
    const received = `Received: ${this.utils.printReceived(eventNames)}`;
    return `${matcherHint}\n\n${expected}\n${received}`;
  };

  return {
    pass,
    message
  }
}
